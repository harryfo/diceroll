document.addEventListener('deviceready', onDeviceReady, false);

var storage = window.localStorage;
var currentDiceValue = { "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6 };
var currentlyRolling = 0;
var readyToRoll = true;
var numberOfDice = 1;
var maxDiceValue = 6;
var numberDisplay = "dots";
var animationSetting = "full";
var animationSpeedSetting = "normal";
var backgroundColor = "multi";
var dotColor = "white";
var soundSetting = true;
var hapticSetting = true;
var tapToRoll = true;
var shakeToRoll = true;
var settingsOpen = false;
var showTotalSetting = false;
var showBorderSetting = false;
var interstitialAd;
var bannerAd;
var version = "1.4";
var getSettingsFromServerTimer;
var adSettings;
var appRateSettings;
var settingsOpenAdCounter = 0;
var settingsCloseAdCounter = 0;
var diceRollAfterAdCounter = 0;
var diceRollBeforeAdCounter = 0;
var rollDiceAfterAd = false;
var interstitialAdLoaded = false;
var defaultAppLanguage = 'en';
var appLanguage = defaultAppLanguage;
var shakeSensitivity = 30;
var consecutiveShakes = 0;
var previousAcceleration = {
    x: null,
    y: null,
    z: null
};
var currentlyShaking = false;
var diceShakingSound;
var motionCounter = 0;
var lowMotionCounter = 0;
var shakingTimer;
var diceSound;
var androidHaptics;
var androidHapticsEnabled = false;
var removeAdsPurchased = true;

function onDeviceReady() {
    screen.orientation.lock('portrait');
    if (device.platform == "Android") {
        StatusBar.overlaysWebView(true);
        NavigationBar.backgroundColorByHexString('#59b3cd', lightNavigationBar = false);
        androidHaptics = window.plugins.deviceFeedback;
        androidHaptics.isFeedbackEnabled(function(feedback) {
            if(feedback.haptic){
                androidHapticsEnabled = true;
            }
        });
    }
    document.addEventListener("resume", onResume, false);
    getSettingsFromServer();
    navigator.globalization.getPreferredLanguage(function(language){
        var twoLetterCode = language['value'].substr(0, 2);
        if (language['value'] == 'en-US' || language['value'] == 'zh-TW'  || language['value'] == 'zh-HK'){
            appLanguage = language['value'];
        } else if (twoLetterCode == 'de' || twoLetterCode == 'en' || twoLetterCode == 'es' || twoLetterCode == 'pt' || twoLetterCode == 'fr' || twoLetterCode == 'it' || twoLetterCode == 'zh'){
            appLanguage = twoLetterCode;
        }
        $('body').addClass(appLanguage);
        changeLanguage(appLanguage, setDiceSettings);
    });
    // initStore();
}

function onResume() {
    getSettingsFromServer();
}

function initStore() {
    if (!window.store) {
        return;
    }
    store.register({
        id:    'removeadsdice',
        type:   store.NON_CONSUMABLE
    });
    store.when('removeadsdice').updated(refreshProductUI);
    store.when('removeadsdice').cancelled(dismissLoadingOverlay);
    store.when('removeadsdice').error(dismissLoadingOverlay);
    store.when('removeadsdice').approved(function(p) {
        p.verify();
    });
    store.when('removeadsdice').verified(finishPurchase);
    store.refresh();
}

function dismissLoadingOverlay(){
    $('.invisibleoverlay').hide();
    $('.removeadsloading').css({'display': 'none'});
    $('.removeadstext').show();
    $('.restorepurchasesloading').css({'display': 'none'});
    $('.restorepurchasestext').show();
};

function refreshProductUI(product) {
    if (product.canPurchase == true) {
        $('.removeadstext').html(getATranslation('removeadssetting') + ' ' + product.price);
        $('.removeads').css({'display' : 'flex'});
        $('.restorepurchases').css({'display' : 'flex'});
        dismissLoadingOverlay();
    } else if (product.owned == true){
        $('.removeads').hide();
        $('.restorepurchases').hide();
        $('.invisibleoverlay').hide();
        removeAdsPurchased = true;
    }
}

function finishPurchase(p){
    removeAdsPurchased = true;
    p.finish();
}

$('.removeads').on('click', function(){
    $('.invisibleoverlay').show();
    $('.removeadsloading').css({'display': 'inline-block'});
    $('.removeadstext').hide();
    store.order('removeadsdice');
});

$('.restorepurchases').on('click', function(){
    $('.invisibleoverlay').show();
    $('.restorepurchasesloading').css({'display': 'inline-block'});
    $('.restorepurchasestext').hide();
    store.refresh().finished(dismissLoadingOverlay);
});

function setDiceSettings(){
    if(storage.getItem('currentDiceValue') != 'null' && storage.getItem('currentDiceValue') != null) {
        currentDiceValue = $.parseJSON(storage.getItem('currentDiceValue'));
    }
    $.each(currentDiceValue, function(i, v){
        $('.dicecontainer-' + i).find('.dice-' + v).css({scaleX: '1', left: '0'});
    });
    navigator.accelerometer.watchAcceleration(deviceMotionDetected, deviceMotionError, {frequency: 1000/10});
    if(storage.getItem('numberOfDice') != 'null' && storage.getItem('numberOfDice') != null) {
        $('.numberofdiceselect').val(storage.getItem('numberOfDice')).change();
    }
    if(storage.getItem('backgroundColor') != 'null' && storage.getItem('backgroundColor') != null) {
        $('.backgroundcolorselect').val(storage.getItem('backgroundColor')).change();
    }
    if(storage.getItem('dotColor') != 'null' && storage.getItem('dotColor') != null) {
        $('.dotcolourselect').val(storage.getItem('dotColor')).change();
    }
    if(storage.getItem('maxDiceValue') != 'null' && storage.getItem('maxDiceValue') != null) {
        maxDiceValue = storage.getItem('maxDiceValue');
        $('.dicerangeselect').val(storage.getItem('maxDiceValue'));
        $('.dicerangevalue').html($( ".dicerangeselect option:selected" ).text());
    }
    if(storage.getItem('numberDisplay') != 'null' && storage.getItem('numberDisplay') != null) {
        $('.numberdisplayselect').val(storage.getItem('numberDisplay')).change();
    }
    if(storage.getItem('animationSetting') != 'null' && storage.getItem('animationSetting') != null) {
        $('.animationselect').val(storage.getItem('animationSetting')).change();
    }
    if(storage.getItem('animationSpeedSetting') != 'null' && storage.getItem('animationSpeedSetting') != null) {
        $('.animationspeedselect').val(storage.getItem('animationSpeedSetting')).change();
    }
    if(storage.getItem('hapticSetting') == 'false' || storage.getItem('hapticSetting') == false) {
        $('.hapticcheckbox').prop('checked', false).change();
    }
    if(storage.getItem('soundSetting') == 'false' || storage.getItem('soundSetting') == false) {
        $('.soundcheckbox').prop('checked', false).change();
    }
    if(storage.getItem('tapToRoll') == 'false' || storage.getItem('tapToRoll') == false) {
        $('.taptorollcheckbox').prop('checked', false).change();
    }
    if(storage.getItem('shakeToRoll') == 'false' || storage.getItem('shakeToRoll') == false) {
        $('.shaketorollcheckbox').prop('checked', false).change();
    }
    if(storage.getItem('showTotalSetting') == 'true' || storage.getItem('showTotalSetting') == true) {
        $('.showtotalcheckbox').prop('checked', true).change();
        showTotal();
    }
    if(storage.getItem('showBorderSetting') == 'true' || storage.getItem('showBorderSetting') == true) {
        $('.showbordercheckbox').prop('checked', true).change();
        showHideBorder();
    }
    $('.launchoverlay').fadeOut(300);
}

function getSettingsFromServer(){
    $.ajax({
        type: 'POST',
        url: 'https://harryfo.com/diceroll/php/settings.php',
        timeout: 3000,
        data: { version: version }
    }).done(function(response){
        processServerSettings(response);
        clearTimeout(getSettingsFromServerTimer);
        getSettingsFromServerTimer = setTimeout(function(){ 
            getSettingsFromServer();
        }, 600000);
    });
}

$('.submenubutton').on('click', function(){
    var subMenuId = $(this).attr('submenuid');
    $('.invisibleoverlay').show();
    $('.settingsmainmenu').animate({left: '-50%'}, 300);
    $('.settingssubmenu[submenuid="' + subMenuId + '"]').addClass('active').animate({left: '0'}, 300, function(){
        $('.invisibleoverlay').hide();
    });
});

$('.backsettingrow').on('click', function(){
    $('.invisibleoverlay').show();
    $('.settingsmainmenu').animate({left: '0%'}, 300);
    $('.settingssubmenu.active').removeClass('active').animate({left: '100%'}, 300, function(){
        $('.invisibleoverlay').hide();
    });
});

function processServerSettings(response){
    if (response.advertisingSettings){
        adSettings = response.advertisingSettings;
        if (adSettings.showAdverts == true && removeAdsPurchased == false){
            initialiseAds(adSettings.interstitialAdUnitIdiOS, adSettings.interstitialAdUnitIdAndroid, adSettings.bannerAdUnitIdiOS, adSettings.bannerAdUnitIdAndroid, adSettings.showBannerAd, adSettings.bannerAdPosition);
        }
    }
    if (response.appRateSettings){
        appRateSettings = response.appRateSettings;
        if (appRateSettings.showAppRateButton == true){
            $('.showrateapp').css({'display': 'flex'});
        } else {
            $('.showrateapp').hide();
        }
        if (appRateSettings.appRatePromptsOn == true){
            initialiseRateAppPrompts(appRateSettings.appStoreUrliOS, appRateSettings.appStoreUrlAndroid, appRateSettings.reviewTypeiOS, appRateSettings.reviewTypeAndroid, appRateSettings.simpleMode, appRateSettings.usesUntilPrompt, appRateSettings.promptAgainForEachNewVersion);
        }
    }
}

$('.diceouter').on('click', function(){
    if (tapToRoll == true && settingsOpen == false){
        startRollDice();
    }
});

function startRollDice(){
    if (adSettings && adSettings.showBeforeDiceRoll == true && interstitialAdLoaded == true && removeAdsPurchased == false){
        diceRollAfterAdCounter++;
        if (diceRollAfterAdCounter == adSettings.showBeforeDiceRollFrequency){
            diceRollAfterAdCounter = 0;
            showInterstitialAd();
            rollDiceAfterAd = true;
        } else {
            rollDice();
        }
    } else {
        rollDice();
    }
}

$('.settingscog').on('click', function(){
    showSettings();
});

$('.settingsoverlay, .closesettings').on('click', function(){
    hideSettings();
});

$('.numberofdiceselect').on('change', function(){
    $('.numberofdicevalue').html($( ".numberofdiceselect option:selected" ).text());
    changeNumberOfDice($('.numberofdiceselect').val());
});

$('.dotcolourselect').on('change', function(){
    $('.dotcolourvalue').html($( ".dotcolourselect option:selected" ).text());
    changeDiceDotColor($('.dotcolourselect').val());
});

$('.backgroundcolorselect').on('change', function(){
    $('.backgroundcolorvalue').html($( ".backgroundcolorselect option:selected" ).text());
    changeDiceBackgroundColor($('.backgroundcolorselect').val());
});

$('.dicerangeselect').on('change', function(){
    $('.dicerangevalue').html($( ".dicerangeselect option:selected" ).text());
    changeDiceRange($('.dicerangeselect').val());
});

$('.numberdisplayselect').on('change', function(){
    $('.numberdisplayvalue').html($( ".numberdisplayselect option:selected" ).text());
    changeNumberDisplay($('.numberdisplayselect').val());
});

$('.animationselect').on('change', function(){
    $('.animationvalue').html($( ".animationselect option:selected" ).text());
    animationSetting = $('.animationselect').val();
    storage.setItem('animationSetting', animationSetting);
});

$('.animationspeedselect').on('change', function(){
    $('.animationspeedvalue').html($( ".animationspeedselect option:selected" ).text());
    animationSpeedSetting = $('.animationspeedselect').val();
    storage.setItem('animationSpeedSetting', animationSpeedSetting);
});

$('.soundcheckbox').on('change', function(){
    soundSetting = $('.soundcheckbox').prop('checked');
    storage.setItem('soundSetting', $('.soundcheckbox').prop('checked'));
});

$('.hapticcheckbox').on('change', function(){
    hapticSetting = $('.hapticcheckbox').prop('checked');
    storage.setItem('hapticSetting', $('.hapticcheckbox').prop('checked'));
});

$('.taptorollcheckbox').on('change', function(){
    tapToRoll = $('.taptorollcheckbox').prop('checked');
    storage.setItem('tapToRoll', $('.taptorollcheckbox').prop('checked'));
});

$('.shaketorollcheckbox').on('change', function(){
    shakeToRoll = $('.shaketorollcheckbox').prop('checked');
    storage.setItem('shakeToRoll', $('.shaketorollcheckbox').prop('checked'));
});

$('.showtotalcheckbox').on('change', function(){
    showTotalSetting = $('.showtotalcheckbox').prop('checked');
    storage.setItem('showTotalSetting', $('.showtotalcheckbox').prop('checked'));
    $('.textoverlay').hide();
});

$('.showbordercheckbox').on('change', function(){
    showBorderSetting = $('.showbordercheckbox').prop('checked');
    storage.setItem('showBorderSetting', $('.showbordercheckbox').prop('checked'));
    showHideBorder();
});

function rollDice(){
    if (readyToRoll == true){
        $('.textoverlay').hide();
        readyToRoll = false;
        readVoiceoverAlert(getATranslation('rollingprompt'));
        var x;
        if (soundSetting == true){
            var diceSoundPath = 'sounds/rolling/rolling' + (1 + Math.floor(Math.random() * 10)) + '.m4a';
            if (device.platform == "Android"){
                diceSoundPath = '/android_asset/www/sounds/rolling/rolling' + (1 + Math.floor(Math.random() * 5)) + '.m4a';
            }
            diceSound = new Media(diceSoundPath);
            diceSound.play();
        }
        for (x = 1; x <= numberOfDice; x++){
            currentlyRolling++;
            var diceSequence = generateDiceSequence(x);
            currentDiceValue[x] = diceSequence[0];
            var animationSpeed = 100;
            if (animationSpeedSetting == "fast"){
                animationSpeed = 30;
            } else if (animationSpeedSetting == "slow"){
                animationSpeed = 200;
            }
            rollAnimation(diceSequence, 1, animationSpeed, x);
        }
    }
}

function rollAnimation(diceSequence, x, animationSpeed, diceContainerNumber){
    if (animationSetting == "full"){
        $('.dicecontainer-' + diceContainerNumber).find('.dice-' + diceSequence[x]).animate({scaleX: '1', left: '0'}, animationSpeed, 'linear');
        $('.dicecontainer-' + diceContainerNumber).find('.dice-' + currentDiceValue[diceContainerNumber]).animate({scaleX: '0', left: '-50%'}, animationSpeed, 'linear', function(){
            prepareNextRollAnimation(diceSequence, x, animationSpeed, diceContainerNumber);
        });
    } else if (animationSetting == "reduced"){
        $('.dicecontainer-' + diceContainerNumber).find('.dice-' + diceSequence[x]).css({scaleX: '1', left: '0'});
        $('.dicecontainer-' + diceContainerNumber).find('.dice-' + currentDiceValue[diceContainerNumber]).css({scaleX: '0', left: '-50%'});
        setTimeout(function(){ prepareNextRollAnimation(diceSequence, x, animationSpeed, diceContainerNumber); }, animationSpeed);
    } else if (animationSetting == "none"){
        $('.rollingtextoverlay').html(getATranslation('rollingprompt'));
        $('.rollingtextoverlay').show();
        setTimeout(function(){
            x = (diceSequence.length - 1);
            $('.dicecontainer-' + diceContainerNumber).find('.diceouter').css({scaleX: '0', left: '50%'});
            $('.dicecontainer-' + diceContainerNumber).find('.dice-' + diceSequence[x]).css({scaleX: '1', left: '0'});
            $('.textoverlay').hide();
            currentDiceValue[diceContainerNumber] = diceSequence[x];
            if (hapticSetting == true){
                playAHaptic("heavy");
            }
            storage.setItem('currentDiceValue', JSON.stringify(currentDiceValue));
            currentlyRolling--;
            setTimeout(function(){ 
                readyToRoll = true;
                if (soundSetting == true){
                    diceSound.release();
                }
            }, 300);
            if (showTotalSetting == true){
                showTotal();
            }
            if (adSettings && adSettings.showAfterDiceRoll == true && removeAdsPurchased == false){
                diceRollBeforeAdCounter++;
                if (diceRollBeforeAdCounter == adSettings.showAfterDiceRollFrequency){
                    diceRollBeforeAdCounter = 0;
                    showInterstitialAd();
                }
            }
            var diceAnnouncementText = getATranslation('valueofdieis');
            if (numberOfDice > 1){
                diceAnnouncementText = getATranslation('valuesofdiceare');
            }
            for (x = 0; x < numberOfDice; x++){
                if (x != 1){
                    diceAnnouncementText += ', ';
                }
                diceAnnouncementText += currentDiceValue[x + 1];
                readVoiceoverAlert(diceAnnouncementText);
            }
        }, (animationSpeed * 4));
    }
}

function prepareNextRollAnimation(diceSequence, x, animationSpeed, diceContainerNumber){
    $('.dicecontainer-' + diceContainerNumber).find('.dice-' + currentDiceValue[diceContainerNumber]).css({scaleX: '0', left: '50%'});
    currentDiceValue[diceContainerNumber] = diceSequence[x];
    x++;
    if (x < diceSequence.length){
        if (hapticSetting == true){
            playAHaptic("light");
        }
        rollAnimation(diceSequence, x, animationSpeed += 30, diceContainerNumber);
    } else {
        if (hapticSetting == true){
            playAHaptic("heavy");
        }
        currentlyRolling--;
        storage.setItem('currentDiceValue', JSON.stringify(currentDiceValue));
        if (showTotalSetting == true && currentlyRolling == 0){
            showTotal();
        }
        if (currentlyRolling == 0){
            setTimeout(function(){ 
                readyToRoll = true;
                if (soundSetting == true){
                    diceSound.release();
                }
            }, 300);
            if (adSettings && adSettings.showAfterDiceRoll == true && removeAdsPurchased == false){
                diceRollBeforeAdCounter++;
                if (diceRollBeforeAdCounter == adSettings.showAfterDiceRollFrequency){
                    diceRollBeforeAdCounter = 0;
                    showInterstitialAd();
                }
            }
            var diceAnnouncementText = getATranslation('valueofdieis');
            if (numberOfDice > 1){
                diceAnnouncementText = getATranslation('valuesofdiceare');
            }
            for (x = 0; x < numberOfDice; x++){
                if (x != 0){
                    diceAnnouncementText += ', ';
                }
                diceAnnouncementText += (' ' + currentDiceValue[x + 1]);
                readVoiceoverAlert(diceAnnouncementText);
            }
        }
    }
}

function showTotal(){
    var diceTotal = 0;
    $.each(currentDiceValue, function(i, v){
        if (numberOfDice >= i){
            diceTotal += v;
        }
    });
    $('.totaltextoverlay').html(getATranslation('totalprompt') + diceTotal);
    $('.totaltextoverlay').show();
}

function showHideBorder(){
    if (showBorderSetting == true){
        $('.diceouter').css({'outline-width': '5px'});
    } else {
        $('.diceouter').css({'outline-width': '0px'});
    }
}

function generateDiceSequence(y){
    var diceResult = 1 + Math.floor(Math.random() * maxDiceValue);
    var numberOfTurns = 2 + Math.floor(Math.random() * 4);
    var diceOptions = [];
    for (x = 1; x <= maxDiceValue; x++){
        diceOptions.push(x);
    }
    var diceSequence = [currentDiceValue[y]];
    var x;
    for (x = 0; x < numberOfTurns; x++){
        var diceOptionToRemove = diceOptions.indexOf(currentDiceValue[y]);
        diceOptions.splice(diceOptionToRemove, 1);
        var nextInSequence = Math.floor(Math.random() * diceOptions.length);
        diceSequence.push(diceOptions[nextInSequence]);
        diceOptions.push(currentDiceValue[y]);
        currentDiceValue[y] = diceOptions[nextInSequence];
    }
    if (diceSequence[diceSequence.length - 1] != diceResult){
        diceSequence.push(diceResult);
        currentDiceValue[y] = diceResult;
    }
    return diceSequence;
}

function showSettings(){
    if (adSettings && adSettings.showOnSettingsOpen == true && removeAdsPurchased == false){
        settingsOpenAdCounter++;
        if (settingsOpenAdCounter == adSettings.showOnSettingsOpenFrequency){
            settingsOpenAdCounter = 0;
            showInterstitialAd();
        }
    }
    $('.settingsoverlay').show();
    $('.settingscontainer').css({'display': 'flex'});
    $('.textoverlay').hide();
    settingsOpen = true;
}

function hideSettings(){
    if (adSettings && adSettings.showOnSettingsClose == true && removeAdsPurchased == false){
        settingsCloseAdCounter++;
        if (settingsCloseAdCounter == adSettings.showOnSettingsCloseFrequency){
            settingsCloseAdCounter = 0;
            showInterstitialAd();
        }
    }
    if (showTotalSetting == true){
        showTotal();
    }
    $('.settingsoverlay').hide();
    $('.settingscontainer').hide();
    settingsOpen = false;
}

function changeNumberOfDice(newNumberOfDice){
    $('.app').find('.dicecontainer').remove();
    var x;
    for (x = 1; x <= newNumberOfDice; x++){
        var newDice = $('.hiddenassets').find('.dicecontainer').clone(true);
        $('.app').prepend(newDice);
        if (newNumberOfDice == 1){
            $('.textnumber').css({fontSize: '35vh'});
            $('.diceinner').css({width: '100vw', height: '100vw'});
            newDice.addClass('dicecontainer-' + x).css({
                height: 'calc(100vh - (env(safe-area-inset-top) + env(safe-area-inset-bottom)))'
            });
        } else if (newNumberOfDice == 2){
            $('.textnumber').css({fontSize: '30vh'});
            newDice.addClass('dicecontainer-' + x).css({height: 'calc(0.5 * (100vh - (env(safe-area-inset-top) + env(safe-area-inset-bottom))))'});
            $('.diceinner').css({width: '80vw', height: '80vw'});
            if (x == 1){
                newDice.find('.diceouter').css({
                    padding: 'env(safe-area-inset-top, 0px) env(safe-area-inset-right, 0px) 0px env(safe-area-inset-left, 0px)',
                    top: 'calc(-1 * env(safe-area-inset-top, 0px))'
                });
            } else if (x == 2){
                newDice.find('.diceouter').css({
                    padding: '0px env(safe-area-inset-right, 0px) env(safe-area-inset-bottom, 0px) env(safe-area-inset-left, 0px)',
                    top: '100%'
                });
            }
        } else if (newNumberOfDice == 3){
            $('.textnumber').css({fontSize: '20vh'});
            newDice.addClass('dicecontainer-' + x).css({height: 'calc(0.3334 * (100vh - (env(safe-area-inset-top) + env(safe-area-inset-bottom))))'});
            $('.diceinner').css({width: '30vh', height: '30vh'});
            if (x == 1){
                newDice.find('.diceouter').css({
                    padding: 'env(safe-area-inset-top, 0px) env(safe-area-inset-right, 0px) 0px env(safe-area-inset-left, 0px)',
                    top: 'calc(-1 * env(safe-area-inset-top, 0px))'
                });
            } else if (x == 2){
                newDice.find('.diceouter').css({
                    top: '100%',
                    padding: '0'
                });
            } else if (x == 3){
                newDice.find('.diceouter').css({
                    padding: '0px env(safe-area-inset-right, 0px) env(safe-area-inset-bottom, 0px) env(safe-area-inset-left, 0px)',
                    top: '200%'
                });
            }
        } else if (newNumberOfDice == 4){
            $('.textnumber').css({fontSize: '20vh'});
            newDice.addClass('dicecontainer-' + x).css({height: 'calc(0.5 * (100vh - (env(safe-area-inset-top) + env(safe-area-inset-bottom))))', width: '50%'});
            $('.diceinner').css({width: '50vw', height: '50vw'});
            if (x == 2 || x == 4){
                newDice.css({left: '50vw'});
            }
            if (x == 1 || x == 2){
                newDice.find('.diceouter').css({
                    padding: 'env(safe-area-inset-top, 0px) env(safe-area-inset-right, 0px) 0px env(safe-area-inset-left, 0px)',
                    top: 'calc(-1 * env(safe-area-inset-top, 0px))'
                });
            } else if (x == 3 || x == 4){
                newDice.find('.diceouter').css({
                    padding: '0px env(safe-area-inset-right, 0px) env(safe-area-inset-bottom, 0px) env(safe-area-inset-left, 0px)',
                    top: '100%'
                });
            }
        } else if (newNumberOfDice == 5){
            $('.textnumber').css({fontSize: '12vh'});
            newDice.addClass('dicecontainer-' + x).css({height: 'calc(0.2 * (100vh - (env(safe-area-inset-top) + env(safe-area-inset-bottom))))'});
            $('.diceinner').css({width: '18vh', height: '18vh'});
            if (x == 1){
                newDice.find('.diceouter').css({
                    padding: 'env(safe-area-inset-top, 0px) env(safe-area-inset-right, 0px) 0px env(safe-area-inset-left, 0px)',
                    top: 'calc(-1 * env(safe-area-inset-top, 0px))'
                });
            } else if (x == 5){
                newDice.find('.diceouter').css({
                    padding: '0px env(safe-area-inset-right, 0px) env(safe-area-inset-bottom, 0px) env(safe-area-inset-left, 0px)',
                    top: '400%'
                });
            } else {
                var amountPushedFromTop = ((x - 1) * 100) + '%';
                newDice.find('.diceouter').css({
                    top: amountPushedFromTop,
                    padding: '0'
                });
            }
        } else if (newNumberOfDice == 6){
            $('.textnumber').css({fontSize: '12vh'});
            newDice.addClass('dicecontainer-' + x).css({height: 'calc(0.3334 * (100vh - (env(safe-area-inset-top) + env(safe-area-inset-bottom))))', width: '50%'});
            $('.diceinner').css({width: '18vh', height: '18vh'});
            if (x == 2 || x == 4 || x == 6){
                newDice.css({left: '50vw'});
            }
            if (x == 1 || x == 2){
                newDice.find('.diceouter').css({
                    padding: 'env(safe-area-inset-top, 0px) env(safe-area-inset-right, 0px) 0px env(safe-area-inset-left, 0px)',
                    top: 'calc(-1 * env(safe-area-inset-top, 0px))'
                });
            } else if (x == 3 || x == 4){
                newDice.find('.diceouter').css({
                    padding: '0px',
                    top: '100%'
                });
            } else if (x == 5 || x == 6){
                newDice.find('.diceouter').css({
                    padding: '0px env(safe-area-inset-right, 0px) env(safe-area-inset-bottom, 0px) env(safe-area-inset-left, 0px)',
                    top: '200%'
                });
            }
        }
        numberOfDice = newNumberOfDice;
        storage.setItem('numberOfDice', numberOfDice);
    }
    $.each(currentDiceValue, function(i, v){
        $('.dicecontainer-' + i).find('.dice-' + v).css({scaleX: '1', left: '0'});
    });
}

function changeDiceBackgroundColor(newBackgroundColor){
    if (newBackgroundColor == 'multi'){
        $('body').css({'background-color': '#FFF'});
        $('.dice-1').css({'background-color' : '#f63f3f'});
        $('.dice-2').css({'background-color' : '#d88d35'});
        $('.dice-4').css({'background-color' : '#8acd59'});
        $('.dice-5').css({'background-color' : '#59b3cd'});
        $('.dice-6').css({'background-color' : '#8c64bd'});
        $('.dice-3').css({'background-color' : '#dad251'});
        $('.dice-7').css({'background-color' : '#dc4dc9'});
        $('.dice-8').css({'background-color' : '#6db278'});
        $('.dice-9').css({'background-color' : '#895f43'});
        if (device.platform == "Android") {
            NavigationBar.backgroundColorByHexString('#59b3cd', lightNavigationBar = false);
        }
        if (dotColor == 'multi'){
            $('.diceouter').css({'outline-color': '#FFF'});
            $('.settingscogblack').hide();
            $('.settingscogwhite').show();
            StatusBar.styleLightContent();
        }
        $('.whitedotsetting').removeAttr('disabled');
        $('.blackdotsetting').removeAttr('disabled');
    } else if (newBackgroundColor == 'white'){
        $('body').css({'background-color': '#FFF'});
        $('.diceouter').css({'background-color': '#FFF'});
        $('.diceouter').css({'outline-color': '#000'});
        $('.settingscogblack').show();
        $('.settingscogwhite').hide();
        StatusBar.styleDefault();
        if (device.platform == "Android") {
            NavigationBar.backgroundColorByHexString('#FFF', lightNavigationBar = true);
        }
        $('.whitedotsetting').attr('disabled','disabled');
        $('.blackdotsetting').removeAttr('disabled');
    } else if (newBackgroundColor == 'black'){
        $('body').css({'background-color': '#000'});
        $('.diceouter').css({'background-color': '#000'});
        $('.diceouter').css({'outline-color': '#FFF'});
        $('.settingscogblack').hide();
        $('.settingscogwhite').show();
        StatusBar.styleLightContent();
        if (device.platform == "Android") {
            NavigationBar.backgroundColorByHexString('#000', lightNavigationBar = false);
        }
        $('.whitedotsetting').removeAttr('disabled');
        $('.blackdotsetting').attr('disabled','disabled');
    }
    backgroundColor = newBackgroundColor;
    storage.setItem('backgroundColor', newBackgroundColor);
}

function changeDiceDotColor(newDotColor){
    if (newDotColor == 'multi'){
        $('.dice-1').find('.dot').css({'background-color' : '#59b3cd'});
        $('.dice-2').find('.dot').css({'background-color' : '#8c64bd'});
        $('.dice-4').find('.dot').css({'background-color' : '#f63f3f'});
        $('.dice-5').find('.dot').css({'background-color' : '#d88d35'});
        $('.dice-6').find('.dot').css({'background-color' : '#dad251'});
        $('.dice-3').find('.dot').css({'background-color' : '#6db278'});
        $('.dice-7').find('.dot').css({'background-color' : '#8acd59'});
        $('.dice-8').find('.dot').css({'background-color' : '#895f43'});
        $('.dice-9').find('.dot').css({'background-color' : '#dc4dc9'});
        $('.dice-1').find('.textnumber').css({'color' : '#59b3cd'});
        $('.dice-2').find('.textnumber').css({'color' : '#8c64bd'});
        $('.dice-4').find('.textnumber').css({'color' : '#f63f3f'});
        $('.dice-5').find('.textnumber').css({'color' : '#d88d35'});
        $('.dice-6').find('.textnumber').css({'color' : '#dad251'});
        $('.dice-3').find('.textnumber').css({'color' : '#6db278'});
        $('.dice-7').find('.textnumber').css({'color' : '#8acd59'});
        $('.dice-8').find('.textnumber').css({'color' : '#895f43'});
        $('.dice-9').find('.textnumber').css({'color' : '#dc4dc9'});
        if (backgroundColor == 'multi'){
            $('.diceouter').css({'outline-color': '#FFF'});
            $('.settingscogblack').hide();
            $('.settingscogwhite').show();
            StatusBar.styleLightContent();
        }
        $('.whitebackgroundsetting').removeAttr('disabled');
        $('.blackbackgroundsetting').removeAttr('disabled');
    } else if (newDotColor == 'white'){
        $('.dot').css({'background-color': '#FFF'});
        $('.textnumber').css({'color': '#FFF'});
        $('.settingscogblack').hide();
        $('.settingscogwhite').show();
        $('.diceouter').css({'outline-color': '#FFF'});
        StatusBar.styleLightContent();
        $('.whitebackgroundsetting').attr('disabled','disabled');
        $('.blackbackgroundsetting').removeAttr('disabled');
    } else if (newDotColor == 'black'){
        $('.dot').css({'background-color': '#000'});
        $('.textnumber').css({'color': '#000'});
        $('.settingscogblack').show();
        $('.settingscogwhite').hide();
        StatusBar.styleDefault();
        $('.diceouter').css({'outline-color': '#000'});
        $('.whitebackgroundsetting').removeAttr('disabled');
        $('.blackbackgroundsetting').attr('disabled','disabled');
    }
    dotColor = newDotColor;
    storage.setItem('dotColor', newDotColor);
}

function changeNumberDisplay(newNumberDisplay){
    if (newNumberDisplay == "dots"){
        $('.dot').show();
        $('.textnumber').hide();
        $('[translationid=numbercoloursettingname]').attr('translationid', 'dotcoloursettingname').html(getATranslation('dotcoloursettingname'));
        $('[translationid=numbercolourselect]').attr('translationid', 'dotcolourselect').attr('aria-label', getATranslation('dotcolourselect'));
    } else if (newNumberDisplay == "number"){
        $('.dot').hide();
        $('.textnumber').show();
        $('[translationid=dotcoloursettingname]').attr('translationid', 'numbercoloursettingname').html(getATranslation('numbercoloursettingname'));
        $('[translationid=dotcolourselect]').attr('translationid', 'numbercolourselect').attr('aria-label', getATranslation('numbercolourselect'));
    }
    numberDisplay = newNumberDisplay;
    storage.setItem('numberDisplay', numberDisplay);
}

function changeDiceRange(newDiceRange){
    maxDiceValue = newDiceRange;
    for (x = 1; x <= numberOfDice; x++){
        generateDiceSequence(x);
    }
    changeNumberOfDice(numberOfDice);
    storage.setItem('maxDiceValue', maxDiceValue);
}

function initialiseAds(interstitialAdUnitIdiOS, interstitialAdUnitIdAndroid, bannerAdUnitIdiOS, bannerAdUnitIdAndroid, showBannerAd, bannerAdPosition){
    admob.start();
    var interstitialAdUnitId = interstitialAdUnitIdiOS;
    var bannerAdUnitId = bannerAdUnitIdiOS;
    if (device.platform == "Android"){
        interstitialAdUnitId = interstitialAdUnitIdAndroid;
        bannerAdUnitId = bannerAdUnitIdAndroid;
    }
    if(!interstitialAd){
        interstitialAd = new admob.InterstitialAd({
            adUnitId: interstitialAdUnitId,
            npa: '1',
        });
        interstitialAd.load();
    }
    if(showBannerAd == true){
        if (!bannerAd){
            bannerAd = new admob.BannerAd({
                adUnitId: bannerAdUnitId,
                position: bannerAdPosition,
                npa: '1',
            });
        }
        bannerAd.show();
    } else {
        if (bannerAd){
            bannerAd.hide();
        }
    }
}

document.addEventListener('admob.interstitial.showfail', function(){
    if (rollDiceAfterAd == true){
        rollDiceAfterAd = false;
        rollDice();
    }
});

document.addEventListener('admob.interstitial.load', function(){
    interstitialAdLoaded = true;
});

document.addEventListener('admob.interstitial.loadfail', function(){
    interstitialAdLoaded = false;
    setTimeout(function(){ 
        interstitialAd.load();
    }, 5000);
});

document.addEventListener('admob.interstitial.dismiss', function() {
    interstitialAd.load();
    if (rollDiceAfterAd == true){
        rollDiceAfterAd = false;
        rollDice();
    }
});

function showInterstitialAd(){
    interstitialAd.show();
}

function showBannerAd(){
    bannerAd.show();
}

function initialiseRateAppPrompts(appStoreUrliOS, appStoreUrlAndroid, reviewTypeiOS, reviewTypeAndroid, simpleMode, usesUntilPrompt, promptAgainForEachNewVersion){
    AppRate.setPreferences({
        storeAppURL: {
            ios: appStoreUrliOS,
            android: appStoreUrlAndroid
        },
        reviewType: {
            ios: reviewTypeiOS,
            android: reviewTypeAndroid
        },
        simpleMode: simpleMode,
        usesUntilPrompt: usesUntilPrompt,
        promptAgainForEachNewVersion: promptAgainForEachNewVersion,
        useLanguage: appLanguage
    });
    AppRate.promptForRating(false);
}

$('.showrateapp').on('click', function(){
    AppRate.promptForRating();
});

function changeLanguage(countryCode, callback){
    $.each(translations[countryCode]['html'], function( translationid, translation){
        $('[translationid=' + translationid + ']').html(translation);
    });
    $.each(translations[countryCode]['aria'], function( translationid, translation){
        $('[translationid=' + translationid + ']').attr('aria-label', translation);
    });
    callback();
}

function getATranslation(translationId){
    if (translations[appLanguage]['html'][translationId]){
        return translations[appLanguage]['html'][translationId];
    } else if (translations[appLanguage]['aria'][translationId]){
        return translations[appLanguage]['aria'][translationId];
    } else if (translations[defaultAppLanguage]['html'][translationId]){
        return translations[defaultAppLanguage]['html'][translationId];
    } else if (translations[defaultAppLanguage]['aria'][translationId]){
        return translations[defaultAppLanguage]['aria'][translationId];
    }
}

function readVoiceoverAlert(textToRead){
    var voiceoverAlert = $('.voiceoveralert').clone();
    $('.voiceoveralert').remove();
    voiceoverAlert.html(textToRead);
    $('body').append(voiceoverAlert);
}

function deviceMotionDetected(acceleration){
    var accelerationChange = {};
    if (previousAcceleration.x !== null) {
        accelerationChange.x = Math.abs(previousAcceleration.x - acceleration.x);
        accelerationChange.y = Math.abs(previousAcceleration.y - acceleration.y);
        accelerationChange.z = Math.abs(previousAcceleration.z - acceleration.z);
    }

    previousAcceleration = {
        x: acceleration.x,
        y: acceleration.y,
        z: acceleration.z
    };

    if ((accelerationChange.x + accelerationChange.y + accelerationChange.z) > shakeSensitivity && currentlyShaking == false && readyToRoll == true && shakeToRoll == true && settingsOpen == false) {
        consecutiveShakes++;
        if (consecutiveShakes == 2){
            consecutiveShakes = 0;
            shakeStartCallback();
        }
    }

    if ((accelerationChange.x + accelerationChange.y + accelerationChange.z) > (shakeSensitivity / 8) && currentlyShaking == true) {
        currentlyShakingCallback();
    }

    if ((accelerationChange.x + accelerationChange.y + accelerationChange.z) < (shakeSensitivity / 8) && currentlyShaking == true) {
        shakeStopCallback();
    }
}

function deviceMotionError(){};

function shakeStartCallback(){
    currentlyShaking = true;
    if (hapticSetting == true){
        playShakingHaptics();
    }
    if (soundSetting == true){
        var diceShakingSoundPath = 'sounds/shaking/shaking' + (1 + Math.floor(Math.random() * 6)) + '.m4a';
        if (device.platform == "Android"){
            diceShakingSoundPath = '/android_asset/www/sounds/shaking/shaking' + (1 + Math.floor(Math.random() * 6)) + '.m4a';
        }
        diceShakingSound = new Media(diceShakingSoundPath);
        diceShakingSound.play();
    }
    shakingTimer = setTimeout(function(){ 
        if (currentlyShaking == true){
            shakeStopCallback();
        }
    }, 2000);
}

function currentlyShakingCallback(){
    motionCounter++;
    if (motionCounter == 5){
        motionCounter = 0;
        if (hapticSetting == true){
            playShakingHaptics();
        }
    }
}

function shakeStopCallback(){
    clearTimeout(shakingTimer);
    if (soundSetting == true){
        diceShakingSound.stop();
        diceShakingSound.release();
    }
    currentlyShaking = false;
    startRollDice();
}

function randomFromArray(array) {
    return array[Math.floor(Math.random() * array.length)];
}

function playAHaptic(weight){
    var androidWeight;
    var iosWeight = weight;
    if (device.platform == "Android"){
        if (weight == "heavy"){
            androidWeight = androidHaptics.VIRTUAL_KEY;
        }
        if (weight == "light"){
            androidWeight = androidHaptics.LONG_PRESS;
        }
    }
    if (weight == "random"){
        iosWeight = randomFromArray(['light', 'medium', 'heavy']);
        if (device.platform == "Android"){
            androidWeight = randomFromArray([androidHaptics.VIRTUAL_KEY, androidHaptics.LONG_PRESS, androidHaptics.KEYBOARD_TAP]);
        }
    }
    if (device.platform == "iOS"){
        TapticEngine.impact({style: iosWeight});
    } else if (device.platform == "Android" && androidHapticsEnabled == true){
        androidHaptics.haptic(androidWeight);
        androidHaptics.acoustic();
    } else if (weight == "heavy") {
        navigator.vibrate(100);
    }
}

function playShakingHaptics(){
    for (x = 1; x <= 6; x++){
        setTimeout(function(){
            playAHaptic("random");
        }, (1 + Math.floor(Math.random() * 500)));
    }
}