import { AdSizeType, MobileAd, MobileAdOptions } from './shared';
declare type Position = 'top' | 'bottom';
export interface BannerAdOptions extends MobileAdOptions {
    position?: Position;
    size?: AdSizeType;
    offset?: number;
}
export default class BannerAd extends MobileAd<BannerAdOptions> {
    private _loaded;
    constructor(opts: BannerAdOptions);
    load(): Promise<unknown>;
    show(): Promise<unknown>;
    hide(): Promise<unknown>;
}
export {};
