import { AdSizeType, Events, NativeActions } from './generated';
export declare type MobileAdOptions = {
    adUnitId: string;
};
export declare class MobileAd {
    private static allAds;
    readonly adUnitId: string;
    readonly id: number;
    constructor({ adUnitId }: MobileAdOptions);
}
export { AdSizeType, Events, NativeActions };
export declare enum MaxAdContentRating {
    G = "G",
    MA = "MA",
    PG = "PG",
    T = "T",
    UNSPECIFIED = ""
}
export declare enum ChildDirectedTreatmentTag {
    FALSE = 0,
    TRUE = 1,
    UNSPECIFIED = -1
}
export declare enum UnderAgeOfConsentTag {
    FALSE = 0,
    TRUE = 1,
    UNSPECIFIED = -1
}
export declare type RequestConfig = {
    maxAdContentRating?: MaxAdContentRating;
    tagForChildDirectedTreatment?: ChildDirectedTreatmentTag;
    tagForUnderAgeOfConsent?: UnderAgeOfConsentTag;
    testDeviceIds?: string[];
};
export declare const enum Platforms {
    android = "android",
    ios = "ios"
}
export declare function execAsync(action: NativeActions, args?: any[]): Promise<unknown>;
export declare function fireDocumentEvent(eventName: string, data?: null): void;
export declare function waitEvent(successEvent: string, failEvent?: string): Promise<CustomEvent>;
