"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.waitEvent = exports.fireDocumentEvent = exports.execAsync = exports.UnderAgeOfConsentTag = exports.ChildDirectedTreatmentTag = exports.MaxAdContentRating = exports.NativeActions = exports.Events = exports.AdSizeType = exports.MobileAd = void 0;
var cordova_1 = require("cordova");
var generated_1 = require("./generated");
Object.defineProperty(exports, "AdSizeType", { enumerable: true, get: function () { return generated_1.AdSizeType; } });
Object.defineProperty(exports, "Events", { enumerable: true, get: function () { return generated_1.Events; } });
Object.defineProperty(exports, "NativeActions", { enumerable: true, get: function () { return generated_1.NativeActions; } });
var MobileAd = /** @class */ (function () {
    function MobileAd(_a) {
        var adUnitId = _a.adUnitId;
        this.adUnitId = adUnitId;
        this.id = 10001 + Object.keys(MobileAd.allAds).length;
        MobileAd.allAds[this.id] = this;
    }
    MobileAd.allAds = {};
    return MobileAd;
}());
exports.MobileAd = MobileAd;
var MaxAdContentRating;
(function (MaxAdContentRating) {
    MaxAdContentRating["G"] = "G";
    MaxAdContentRating["MA"] = "MA";
    MaxAdContentRating["PG"] = "PG";
    MaxAdContentRating["T"] = "T";
    MaxAdContentRating["UNSPECIFIED"] = "";
})(MaxAdContentRating = exports.MaxAdContentRating || (exports.MaxAdContentRating = {}));
var ChildDirectedTreatmentTag;
(function (ChildDirectedTreatmentTag) {
    ChildDirectedTreatmentTag[ChildDirectedTreatmentTag["FALSE"] = 0] = "FALSE";
    ChildDirectedTreatmentTag[ChildDirectedTreatmentTag["TRUE"] = 1] = "TRUE";
    ChildDirectedTreatmentTag[ChildDirectedTreatmentTag["UNSPECIFIED"] = -1] = "UNSPECIFIED";
})(ChildDirectedTreatmentTag = exports.ChildDirectedTreatmentTag || (exports.ChildDirectedTreatmentTag = {}));
var UnderAgeOfConsentTag;
(function (UnderAgeOfConsentTag) {
    UnderAgeOfConsentTag[UnderAgeOfConsentTag["FALSE"] = 0] = "FALSE";
    UnderAgeOfConsentTag[UnderAgeOfConsentTag["TRUE"] = 1] = "TRUE";
    UnderAgeOfConsentTag[UnderAgeOfConsentTag["UNSPECIFIED"] = -1] = "UNSPECIFIED";
})(UnderAgeOfConsentTag = exports.UnderAgeOfConsentTag || (exports.UnderAgeOfConsentTag = {}));
function execAsync(action, args) {
    return new Promise(function (resolve, reject) {
        cordova_1.exec(resolve, reject, generated_1.NativeActions.Service, action, args);
    });
}
exports.execAsync = execAsync;
function fireDocumentEvent(eventName, data) {
    if (data === void 0) { data = null; }
    var event = new CustomEvent(eventName, { detail: data });
    document.dispatchEvent(event);
}
exports.fireDocumentEvent = fireDocumentEvent;
function waitEvent(successEvent, failEvent) {
    if (failEvent === void 0) { failEvent = ''; }
    return new Promise(function (resolve, reject) {
        document.addEventListener(successEvent, function (event) {
            resolve(event);
        }, false);
        if (failEvent) {
            document.addEventListener(failEvent, function (failedEvent) {
                reject(failedEvent);
            }, false);
        }
    });
}
exports.waitEvent = waitEvent;
